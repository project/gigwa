<?php

/**
 * @file
 * Gigwa administrative help page.
 */
?>

<h3>About Gigwa</h3>
<p>
  The Gigwa application, which stands for "Genotype Investigator for Genome-Wide
  Analyses", provides an easy and intuitive way to explore large amounts of
  genotyping data by filtering it not only on the basis of variant features,
  including functional annotations, but also matching genotype patterns. It is a
  fairly lightweight, web-based, platform-independent solution that may be
  deployed on a workstation or as a data portal. It allows to feed a MongoDB
  database with VCF, PLINK or HapMap files containing up to tens of billions of
  genotypes, and provides a user-friendly interface to filter data in real time.
  Gigwa provides the means to export filtered data into several popular formats
  and features connectivity not only with online genomic tools, but also with
  standalone software such as FlapJack or IGV. Additionnally, Gigwa-hosted
  datasets are interoperable via two standard REST APIs: GA4GH and BrAPI.
</p>
Project homepage: <a href="http://southgreen.fr/content/gigwa">http://southgreen.fr/content/gigwa</a><br/>
GitHub: <a href="https://github.com/SouthGreenPlatform/Gigwa2">https://github.com/SouthGreenPlatform/Gigwa2</a><br/>
<br/>
<dl>
  <dt>If you use it, please cite:</dt>
  <dd>Sempéré G, Philippe F, Dereeper A, Ruiz M, Sarah G, Larmande P.
  <a href="https://academic.oup.com/gigascience/article/5/1/s13742-016-0131-8/2720987">
  Gigwa-Genotype investigator for genome-wide analyses.</a>
  Gigascience. 2016;5:25.</dd>
</dl>
For any inquiries please contact <a href="mailto:gigwa@cirad.fr">gigwa@cirad.fr</a><br/>
<br/>

<h3>Gigwa Module for Drupal</h3>
<p>
  This extension module for Drupal <b>does not include Gigwa engine</b>.
  You can get Gigwa from <a href="http://southgreen.fr/content/gigwa">here</a>.
  This module only provides an easy way to include Gigwa interface into an
  existing Drupal site.
</p>

<h3>Installation and configuration</h3>
Configuration URL: <?php echo l('settings', 'admin/tripal/extension/gigwa'); ?>
<p>
First, you must install a Gigwa server and load your data as explained in the
related links and video demos <a href="http://southgreen.fr/content/gigwa">there
</a>.
</p>
<p>
  Then, you must configure the <i>Gigwa server URL</i>. It must be the public
  URL to your Gigwa server. You should not include the protocol (just remove
  what is before the "//" in your URL). The URL must include the trailing slash.
  <br/>
  For instance "//gigwa.myserver.com/gigwa/"<br/>
  It is possible to use a Gigwa server that is proxy-ed on your PHP web server
  running Drupal in order to have a common server name. This could be possible
  if you run your Drupal site in a subdirectory of your server. For instance,
  your Drupal site is on "https://www.myserver.com/drupal/" and you Gigwa server
  is proxy-ed at "https://www.myserver.com/gigwa/". In this case, you can use
  the Gigwa URL "/gigwa/" instead of "//gigwa.myserver.com/gigwa/".
</p>
<p>
  Once the Gigwa server URL as been set, you will have to add bank settings.
  Gigwa can search on banks you loaded. You can provide a limited list of banks
  for your Drupal users but be aware that other banks loaded into Gigwa may be
  accessed by advanced users if they know those bank names or access Gigwa
  server directly (ie. not through your Drupal site).<br/>
  Bank settings are just a convenient way to give human-readable names to your
  banks and provide an easy bank selection interface to your users.
</p>
<p>
  Finally, you will have to add a link to your Gigwa page on your site (path:
  "<?php echo l('gigwa', 'gigwa'); ?>") and add the
  <?php echo l('"Use Gigwa Drupal interface" permission', 'admin/people/permissions', array('fragment' => 'module-gigwa')); ?>
  to the appropriate roles (anonymous users, authenticated users, etc.).
</p>
