<?php

/**
 * @file
 * Gigwa user interface.
 */
  $settings = variable_get('gigwa', array());

  if (empty($settings)
      || empty($settings['url'])
      || empty($settings['banks'])) {
?>
    <p>Gigwa has not been configured for this site.</p>
<?php
    return;
  }

  drupal_add_library('system', 'ui.resizable');
  $query_parameters = drupal_get_query_parameters();
  $banks = array();
  foreach ($settings['banks'] as $bank) {
    $banks[$bank['machinename']] = $bank;
  }

  $active_bank = $settings['default_bank'] ?: $settings['banks'][0]['machinename'];
  if (!empty($query_parameters['gigwa_bank'])
      && array_key_exists($query_parameters['gigwa_bank'], $banks)) {
    $active_bank = $query_parameters['gigwa_bank'];
  }

  // Get individual parameter id set in URL.
  // Ex.: ...&individualSubSet=ITC0063;ITC0310;ITC1220.
  $individual_subset = '';
  if (!empty($query_parameters['gigwa_subset'])) {
    $individual_subset = '&individualSubSet=' . $query_parameters['gigwa_subset'];
  }

  $gigwa_frame_url =
    $settings['url']
    . "?hideNavBar=true&amp;module=$active_bank$individual_subset";
?>
<a href="https://gigascience.biomedcentral.com/articles/10.1186/s13742-016-0131-8" title="Gigwa publication" target="_blank"><img src="/gigwa/images/logo.png" title="Gigwa" alt="Gigwa"/></a>
<form>
  <div id="gigwa_module_selector" style="display: none;">
    <select id="gigwa_module_selector_select">
      <?php
        foreach ($banks as $bank_machinename => $bank) {
          if ($active_bank == $bank_machinename) {
            $selected = ' selected="selected"';
          }
          else {
            $selected = '';
          }
          echo
            "    <option value=\"$bank_machinename\"$selected>"
            . $bank['name']
            . "</option>\n";
        }
      ?>
    </select>
  </div>
  <div id="gigwa_project_selector" style="display: none;">
    <label for="gigwa_project_selector_select">Project:</label>
    <select id="gigwa_project_selector_select">
    </select>
  </div>
</form>
<br/>
<div id="gigwa_container">
  <iframe id="gigwa"  class="ui-widget-content" src="<?php echo $gigwa_frame_url; ?>">Your browser does not support iframes. Click on this <a src="<?php echo $gigwa_frame_url; ?>">Gigwa link</a> in order to access Gigwa in degraded mode.</iframe>
</div>
<script>
  var gigwa_bank_settings = <?php echo json_encode($banks); ?>;

  jQuery(function() {
    var $gigwa_container = jQuery("#gigwa_container");
    var $gigwa = jQuery("#gigwa");
    jQuery("#gigwa_module_selector")
      .show()
      .find('> select')
      .on('change', function() {
        // Replace "module=..." value in URL.
        var module_offset = $gigwa.attr('src').indexOf('module=') + 7;
        var next_param_offset = $gigwa.attr('src').indexOf('&', module_offset);
        if (next_param_offset < 0) {
          next_param_offset = $gigwa.attr('src').length;
        }
        url = $gigwa.attr('src')
          .substr(0, module_offset)
            + jQuery("#gigwa_module_selector > select").val()
            + $gigwa.attr('src').substr(next_param_offset)
        ;
        // Remove "project=..." from URL.
        var project_offset = url.indexOf('project=');
        if (project_offset >= 0) {
          if ((project_offset > 0) && ('&' == url.charAt(project_offset - 1))) {
            --project_offset;
          }
          next_param_offset = url.indexOf('&', project_offset + 1);
          if (next_param_offset < 0) {
            next_param_offset = url.length;
          }
          else if ((project_offset > 0)
            && ('?' == url.charAt(project_offset - 1))
            && (next_param_offset + 1 <= url.length)) {
            ++next_param_offset;
          }
          url = url
            .substr(0, project_offset)
            + url.substr(next_param_offset)
          ;
        }

        // Hide and remove previous project selection dropdown.
        jQuery("#gigwa_project_selector").hide();
        var $project_select = jQuery("#gigwa_project_selector > select");
        $project_select.hide().empty();
        // Check if active module has projects.
        var active_module = jQuery("#gigwa_module_selector > select").val();
        if (gigwa_bank_settings[active_module]
            && (1 < gigwa_bank_settings[active_module].projects.length)) {
          gigwa_bank_settings[active_module].projects.forEach(function (project) {
            // Add a new selection item.
            $project_select.append(
              '<option value="'
              + project['name'].replace(/"/g, '_')
              + '">'
              + project['name']
              + '</option>'
            );
          });
          // Make it visible again.
          jQuery("#gigwa_project_selector").show();
          $project_select.show();
          url =
            url
            + '&project='
            + gigwa_bank_settings[active_module].projects[0]['name']
          ;
        }

        $gigwa.attr('src', url);
      })
    ;
    
    // Handle projects.
    jQuery("#gigwa_project_selector > select")
      .on('change', function(e) {
        e.preventDefault();
        // Make sur the dropdown is visible.
        if (!jQuery(this).is(':visible')) {
          return;
        }
        // Replace "project=..." value in URL.
        var project_offset = $gigwa.attr('src').indexOf('project=') + 8;
        if (project_offset < 8) {
          // Not found.
          url = $gigwa.attr('src')
            + '&project='
            + jQuery("#gigwa_project_selector > select").val()
          ;
        }
        else {
          var next_param_offset = $gigwa.attr('src').indexOf('&', project_offset);
          if (next_param_offset < 0) {
            next_param_offset = $gigwa.attr('src').length;
          }
          url = $gigwa.attr('src')
            .substr(0, project_offset)
              + jQuery("#gigwa_project_selector > select").val()
              + $gigwa.attr('src').substr(next_param_offset)
          ;
        }
        $gigwa.attr('src', url);
      })
    ;
    $gigwa_container.resizable();
  });
</script>
