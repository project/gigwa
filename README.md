Gigwa Extension module
======================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * Partnership


INTRODUCTION
------------

This extension module for Drupal does not include Gigwa engine. It only provides
an easy way to include Gigwa interface into an existing Drupal site.

The Gigwa application, which stands for "Genotype Investigator for Genome-Wide
Analyses", provides an easy and intuitive way to explore large amounts of
genotyping data by filtering it not only on the basis of variant features,
including functional annotations, but also matching genotype patterns. It is a
fairly lightweight, web-based, platform-independent solution that may be
deployed on a workstation or as a data portal. It allows to feed a MongoDB
database with VCF, PLINK or HapMap files containing up to tens of billions of
genotypes, and provides a user-friendly interface to filter data in real time.
Gigwa provides the means to export filtered data into several popular formats
and features connectivity not only with online genomic tools, but also with
standalone software such as FlapJack or IGV. Additionnally, Gigwa-hosted
datasets are interoperable via two standard REST APIs: GA4GH and BrAPI.

Project homepage: http://southgreen.fr/content/gigwa
GitHub: https://github.com/SouthGreenPlatform/Gigwa2
Publication: Sempéré G, Philippe F, Dereeper A, Ruiz M, Sarah G, Larmande P.
  Gigwa-Genotype investigator for genome-wide analyses. Gigascience. 2016;5:25.
  https://academic.oup.com/gigascience/article/5/1/s13742-016-0131-8/2720987

For any inquiries please contact "gigwa@cirad.fr".


REQUIREMENTS
------------

This module requires an external Gigwa server to be installed, configured,
loaded and running.
See: http://southgreen.fr/content/gigwa


INSTALLATION
------------

 * Installation and configuration a Gigwa server (see Requirements above).

 * Enable the Drupal module as usual


CONFIGURATION
------------- 

 * Go to the configuration page at /admin/tripal/extension/gigwa

 * Set a "Gigwa server URL". It must be the public URL to your Gigwa
   server. You should not include the protocol (just remove what is before the
   "//" in your URL). The URL must include the trailing slash.
    For instance "//gigwa.myserver.com/gigwa/"
    It is possible to use a Gigwa server that is proxy-ed on your PHP web server
    running Drupal in order to have a common server name. This could be possible
    if you run your Drupal site in a subdirectory of your server. For instance,
    your Drupal site is on "https://www.myserver.com/drupal/" and you Gigwa server
    is proxy-ed at "https://www.myserver.com/gigwa/". In this case, you can use
    the Gigwa URL "/gigwa/" instead of "//gigwa.myserver.com/gigwa/".

 * Add at least one bank setting.
   Gigwa can search on banks you loaded. You can provide a limited list of banks
   for your Drupal users but be aware that other banks loaded into Gigwa may be
   accessed by advanced users if they know those bank names or access Gigwa
   server directly (ie. not through your Drupal site).
   Bank settings are just a convenient way to give human-readable names to your
   banks and provide an easy bank selection interface to your users.

 * Add a link to your Gigwa page on your site (path: "gigwa") for your users.

 * Configure the permission "Use Gigwa Drupal interface" at
   "admin/people/permissions#module-gigwa"


MAINTAINERS
-----------

Current Drupal module maintainer:

 * Valentin Guignon (vguignon) - https://www.drupal.org/user/423148


PARTNERSHIP
-----------
Gigwa has been developed by a team at the South Green Bioinformatics platform.
The lead developer and maintainer is Guilhem Sempéré (guilhem.sempere@cirad.fr).
