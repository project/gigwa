<?php

/**
 * @file
 * Contains the functions used for administration of the Gigwa module.
 */

/**
 * Administrative settings form.
 *
 * @return string
 *   the HTML content of the administration form.
 */
function gigwa_admin_form($form, &$form_state, $no_js_use = FALSE) {
  // Get current settings.
  $settings = variable_get('gigwa', array());

  $form = array();

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Gigwa server URL'),
    '#title_display' => 'before',
    '#description' => t('Only include "http" protocol if "https" is not enabled on Gigwa server. Prefer a URL like "://gigwa.server.com/gigwa/". Gigwa server URL should end with a slash.'),
    '#default_value' => $settings['url'],
    '#size' => 80,
    '#required' => TRUE,
  );

  $form['banks'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Gigwa available banks'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  if (empty($form_state['bank_count'])) {
    $form_state['bank_count'] = count($settings['banks']);
  }
  if (!isset($form_state['project_count'])) {
    $form_state['project_count'] = array();
  }

  $form['banks']['bank_list'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="gigwa-bank-fieldset-wrapper">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );

  for ($i = 0; $i < $form_state['bank_count']; ++$i) {
    $form['banks']['bank_list'][$i] = array(
      '#type' => 'fieldset',
      '#title' => t('Bank settings'),
    );
    $default_value = !empty($settings['banks'][$i]['machinename']) ?
      $settings['banks'][$i]['machinename']
      : '';
    $form['banks']['bank_list'][$i]['machinename'] = array(
      '#type' => 'textfield',
      '#title' => t('Bank machine (internal) name'),
      '#title_display' => 'before',
      '#description' => t('Gigwa bank machine name used in URLs.'),
      '#default_value' => $default_value,
      '#required' => FALSE,
      '#size' => 80,
      '#maxlength' => 128,
    );
    $default_value = !empty($settings['banks'][$i]['name']) ?
      $settings['banks'][$i]['name']
      : '';
    $form['banks']['bank_list'][$i]['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Bank name'),
      '#title_display' => 'before',
      '#default_value' => $default_value,
      '#required' => FALSE,
      '#size' => 80,
      '#maxlength' => 128,
    );

    // Add project selection.
    $form['banks']['bank_list'][$i]['projects']['project_list'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Available projects'),
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    );
    $form['banks']['bank_list'][$i]['projects']['project_list'] = array(
      '#type' => 'container',
      '#prefix' => '<div id="gigwa-bank-' . $i . '-projects-wrapper">',
      '#suffix' => '</div>',
      '#title' => t('Projects'),
      '#tree' => TRUE,
    );

    if (!isset($form_state['project_count'][$i])) {
      if (!empty($settings['banks'][$i]['projects'])) {
        $form_state['project_count'][$i] = count($settings['banks'][$i]['projects']);
      }
      else {
        $form_state['project_count'][$i] = 0;
      }
    }

    for ($j = 0; $j < $form_state['project_count'][$i]; ++$j) {
      $default_value =
        !empty($settings['banks'][$i]['projects'][$j]['name']) ?
        $settings['banks'][$i]['projects'][$j]['name']
        : '';
      $form['banks']['bank_list'][$i]['projects']['project_list'][$j]['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Project (machine) name'),
        '#title_display' => 'before',
        '#default_value' => $default_value,
        '#required' => FALSE,
        '#size' => 80,
        '#maxlength' => 128,
      );
    }
    $form['banks']['bank_list'][$i]['projects']['project_list']['add_project'] = array(
      '#type' => 'submit',
      '#value' => t('Add a project'),
      '#submit' => array('gigwa_add_project_add_one'),
      '#name' => 'add_project_bank_' . $i,
      '#bank' => $i,
      '#ajax' => array(
        'callback' => 'gigwa_add_project_callback',
        'wrapper' => 'gigwa-bank-' . $i . '-projects-wrapper',
      ),
    );

    // @todo: Add radio button for default bank.
  }

  $form['banks']['add_bank'] = array(
    '#type' => 'submit',
    '#value' => t('Add a bank'),
    '#submit' => array('gigwa_add_bank_add_one'),
    '#ajax' => array(
      'callback' => 'gigwa_add_bank_callback',
      'wrapper' => 'gigwa-bank-fieldset-wrapper',
    ),
  );

  // Save button.
  $form['save_settings'] = array(
    '#type'        => 'submit',
    '#value'       => t('Save configuration'),
    '#submit'      => array('gigwa_admin_form_submit'),
  );

  return $form;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function gigwa_add_bank_callback($form, $form_state) {
  return $form['banks']['bank_list'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function gigwa_add_bank_add_one($form, &$form_state) {
  $form_state['bank_count']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function gigwa_add_project_callback($form, $form_state) {
  $i = $form_state['triggering_element']['#bank'];
  return $form['banks']['bank_list'][$i]['projects']['project_list'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function gigwa_add_project_add_one($form, &$form_state) {
  $i = $form_state['triggering_element']['#bank'];
  if (!isset($form_state['project_count'][$i])) {
    $form_state['project_count'][$i] = 0;
  }
  $form_state['project_count'][$i]++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Saves Gigwa configuration changes.
 */
function gigwa_admin_form_submit($form_id, &$form_state) {
  // Get current settings.
  $settings = variable_get('gigwa', array());

  $settings['url'] = $form_state['values']['url'];

  $settings['banks'] = array();
  foreach ($form_state['values']['bank_list'] as $bank) {
    if (!empty($bank['machinename'])) {
      $settings['banks'][] = array(
        'machinename' => $bank['machinename'],
        'name' => $bank['name'],
        'projects' => array_values(
          array_filter(
            $bank['projects']['project_list'],
            function($p) {
              return !empty($p['name']);
            }
          )
        ),
      );
    }
  }

  // Save Gigwa settings.
  variable_set('gigwa', $settings);

  drupal_set_message(t('Gigwa configuration saved.'));
}
